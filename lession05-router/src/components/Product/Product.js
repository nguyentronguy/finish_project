import React, {Component} from 'react';
import Item from "./Item";
import myData from './data.json';
const queryString = require('query-string');

class Product extends Component {
  format_currency = (price) => {
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  render() {
    const query = this.props.location.search;
    const obj = queryString.parse(query);
    console.log(obj)
    return (
      <div id="products" className="row list-group">
        {
          myData.filter(p => obj.type === undefined || p.type === obj.type).map((val, key) => {
            return <Item pid={val.id} price={this.format_currency(val.price)} images={val.images} key={key}>{val.name}</Item>
          })
        }
      </div>
    );
  }
}

export default Product;