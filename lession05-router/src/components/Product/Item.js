import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Item extends Component {
  to_slug = (str) =>{
    str = str.toLowerCase();
    str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
    str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
    str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
    str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
    str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
    str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
    str = str.replace(/(đ)/g, 'd');
    str = str.replace(/([^0-9a-z-\s])/g, '');
    str = str.replace(/(\s+)/g, '-');
    str = str.replace(/^-+/g, '');
    str = str.replace(/-+$/g, '');
    return str;
  };
  render() {
    console.log(this.props);
    return (
      <div className="item col-xs-6 col-lg-4 col-md-4">
        <div className="thumbnail">
          <img className="group list-group-image ojf" src={this.props.images} alt=""/>
          <div className="caption">
            <h4 className="group inner list-group-item-heading">
              {this.props.children}
            </h4>
            <p className="group inner list-group-item-text"> Course of studying DEVELOP to create Website by PHP
            </p>
            <div className="row">
              <div className="col-xs-12 col-md-6">
                <p className="lead">
                  {this.props.price} VND
                </p>
              </div>
              <div className="col-xs-12 col-md-4">
                <Link className="btn btn-success" to={"detail/" + this.props.pid + '/' + this.to_slug(this.props.children) +".html"}>Detail
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Item;