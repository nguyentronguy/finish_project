import React, {Component} from 'react';
import './App.css';
import Footer from "../Footer/Footer";
import Nav from "../Nav/Nav";
import RouterURL from "../RouterURL/RouterURL";
import {BrowserRouter as Router} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Nav/>
          <div className="container" id="main-content">
            <RouterURL/>
            <Footer/>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
