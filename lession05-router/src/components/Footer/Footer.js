import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <div>
        <footer>
          <p className="ta-ct">Địa chỉ: 91c Nguyễn Đình Chiểu, khu phố 2, phường 8, thành phố Bến Tre, tỉnh Bến Tre.</p>
          <p className="ta-ct">Điện thoại: 0275 3824090- 0909290435- 0937806375</p>
        </footer>
      </div>
    );
  }
}

export default Footer;