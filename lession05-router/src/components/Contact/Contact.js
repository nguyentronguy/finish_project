import React, {Component} from 'react';
import {Prompt, Redirect} from "react-router-dom";

const subject = [
  "PHP",
  "ASP",
  "Android",
  "React",
  "Javscript",
  "Angular"
];

class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBlocking: false,
      isRedirect: false,
      txtName: '',
      txtEmail: '',
      txtPhone: '',
      txtMessage: '',
      sltCity: '',
      rdoGioiTinh: 1,
      chkSubject: new Set(),
      file: ''
    };
  }

  checkBoxSubject = () => {
    const monhoc = subject.map((value, key) => {
      return <label className="CHeckbox-inline" key={key}><input
        type="checkbox" value={value} onChange={() => this.checkedSubject(value)}
        checked={this.state.chkSubject.has(value)}/>{value}</label>
    });
    return monhoc
  }

  componentWillMount() {
    this.checkedCheckBox = new Set();
  }

  checkedSubject = (monhoc) => {
    if (this.checkedCheckBox.has(monhoc)) {
      this.checkedCheckBox.delete(monhoc);
    } else {
      this.checkedCheckBox.add(monhoc);
    }

    this.setState({
      chkSubject: this.checkedCheckBox
    });
  }

  isInputChange = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      isBlocking: target.value.length > 0,
      [name]: value
    });
  }
  isFileChange = (event) => {
    this.setState({
      file: event.target.files[0].name
    });
  }

  submitForm = (event) => {
    event.preventDefault();
    event.target.reset();
    this.setState({
      isBlocking: false,
      isRedirect: false
    });
    const {txtName, txtMessage, txtPhone, txtEmail, file, chkSubject, rdoGioiTinh, sltCity} = this.state
    var gioiTinh = '';
    if (parseInt(rdoGioiTinh, 10) === 1) {
      gioiTinh = "Nam"
    } else {
      gioiTinh = "Nu"
    }

    var mhoc = '';
    for (const mh of chkSubject) {
      mhoc += mh + ",";
    }

    var content = '';
    content += 'Ho ten: ' + txtName;
    content += '- Email: ' + txtEmail;
    content += '- Phone: ' + txtPhone;
    content += '- Message: ' + txtMessage;
    content += '- City: ' + sltCity;
    content += '- Sex: ' + gioiTinh;
    content += '- Subject: ' + mhoc;
    content += '- file ' + file;
    console.log(content);
  }

  render() {
    if (this.state.isRedirect) {
      return (<Redirect to="/Home"/>
      )
    }

    return (
      <div>
        <Prompt when={this.state.isBlocking}
                message={location => (`Are you sure you want to go to ${location.pathname}`)}> </Prompt>
        <div className="well well-sm">
          <h3><strong>Contact</strong></h3>
        </div>
        <div className="col-md-7">
          <img title="This is a unique title"
               src="http://dongtrunghathaovivi.com/wp-content/uploads/2017/09/12-cong-dung.png"
               className="framwork" alt=""/>
        </div>
        <div className="col-md-5">
          <h4><strong>Get in touch</strong></h4>
          <form onSubmit={(e) => this.submitForm(e)}>
            <div className="form-group">
              <input type="text" name="txtName" value={this.state.txtName} className="form-control" placeholder="Name"
                     onChange={(event) => this.isInputChange(event)}/>
            </div>
            <div className="form-group">
              <input type="email" name="txtEmail" value={this.state.txtEmail} className="form-control"
                     placeholder="E-mail"
                     onChange={(event) => this.isInputChange(event)}/>
            </div>
            <div className="form-group">
              <input type="tel" name="txtPhone" value={this.state.txtPhone} className="form-control" placeholder="Phone"
                     onChange={(event) => this.isInputChange(event)}/>
            </div>
            <div className="form-group">
              <textarea name="txtMessage" defaultValue={this.state.txtMessage} className="form-control" rows={3}
                        placeholder="Message"
                        onChange={(event) => this.isInputChange(event)}/>
            </div>
            <div className="form-group">
              <select name="sltCity" className="form-control" value={this.state.sltCity}
                      onChange={(event) => this.isInputChange(event)}>
                <option value="">Vui long chon thanh pho</option>
                <option value="hn"> Ha Noi</option>
                <option value="dn"> Da Nang</option>
                <option value="hcm">Ho Chi Minh</option>
              </select>
            </div>
            <div className="form-group">
              {this.checkBoxSubject()}
            </div>
            <div className="form-group">
              <label className="radio-inline"><input type="radio" name="rdoGioiTinh" value="1"
                                                     onChange={(event) => this.isInputChange(event)}
                                                     checked={parseInt(this.state.rdoGioiTinh, 10) === 1}/>Nam</label>
              <label className="radio-inline"><input type="radio" name="rdoGioiTinh" value="2"
                                                     onChange={(event) => this.isInputChange(event)}
                                                     checked={parseInt(this.state.rdoGioiTinh, 10) === 2}/>Nu</label>
            </div>
            <div className="form-group">
              <label className="custom-file">
                <input type="file" id="file" className="custom-file-input" name="fAvatar"
                       onChange={(event) => this.isFileChange(event)}/>
                <span className="custom-file-control"/>
              </label>
            </div>
            <button className="btn btn-default" type="submit" name="button">
              <i className="fa fa-paper-plane-o" aria-hidden="true"/> Submit
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default Contact;