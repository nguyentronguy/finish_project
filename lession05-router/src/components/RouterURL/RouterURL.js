import React, {Component} from 'react';
import {Route,Switch} from 'react-router-dom';
import Home from "../Home/Home";
import Product from "../Product/Product";
import Contact from "../Contact/Contact";
import Detail from "../Detail/Detail";

class RouterURL extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/product" component={Product}/>
          <Route path="/contact" component={Contact}/>
          <Route path="/detail/:id/:slug.html" component={Detail}/>
          <Route component={Home}/>
        </Switch>
      </div>
    );
  }
}

export default RouterURL;