import React, {Component} from 'react';
import {Link} from "react-router-dom";

class Course extends Component {
  render() {
    return (
      <div >
        <h2> {this.props.title}</h2>
        <p>{this.props.description.replace('[title]', this.props.title)}</p>
        <p><Link className="btn btn-success" to="/product?type=mat-ong" role="button">Chi tiết</Link></p>
      </div>
    );
  }
}

export default Course;