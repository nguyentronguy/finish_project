import React, {Component} from 'react';
import Course from "./Course/Course";

class Home extends Component {
  render() {
    return (
      <div className="row row-offcanvas row-offcanvas-right">
        <div className="col-xs-12 col-sm-8">
          <div className="jumbotron ta-ct">
            <h2> Đông trùng hạ thảo </h2>
            <h1> Vi Vi</h1>
            <p> Sức khỏe cho người Việt</p>
            <br/>
          </div>
          <div className="col-xs-6 col-sm-6 col-lg-6 bg">
            <Course title={'Đông trùng hạ thảo ngâm mật ong'}
                    description={'Bồi bổ cơ thể, giữ gìn nhan sắc cho phái nữ, điều hòa huyết áp, phòng chống ung thư... (Xem chi tiết)'}/>
            <Course title={'100% Mật ong  nguyên chất'}
                    description={'Rất tốt cho mạch máu, hỗ trợ sức khỏe... (Xem chi tiết)'}/>
          </div>
          <div className="col-xs-6 col-sm-6 col-lg-6 bg">
            <Course title={'Đông trùng hạ thảo ngâm rượu gạo'}
                    description={'Tăng cường sức khỏe nam giới, cải thiện chức năng gan, thận, phòng chống ung thư ... (Xem chi tiết)'}/>
            <Course title={'Đông trùng hạ thảo sấy khô'}
                    description={'Bồi bổ sức khỏe phòng chống ung thư... (Xem chi tiết)'}/>
          </div>
        </div>
        <div className="col-xs-6 col-sm-4 sidebar-offvanvas" id="sidebar">
          <div className="list-group">
            <a href="/" className="list-group-item">Chứng nhận Vệ sinh an toàn thực phẩm</a>
            <a href="/" className="list-group-item">Phiếu kết quả kiểm nghiệm hoạt tính ĐTHT </a>
            <a href="/" className="list-group-item">Phiếu kiểm tra chất lượng rượu gạo</a>
            <a href="/" className="list-group-item">Phiếu kiểm tra chất lượng mật ong</a>
            <a href="/" className="list-group-item">Công bố sản phẩm ĐTHT Vi VI- Rượu</a>
            <a href="/" className="list-group-item">Công bố sản phẩm ĐTHT Vi VI- Mật ong</a>
            <a href="/" className="list-group-item">Báo cáo khẳng định tác dụng của ĐTHT trên cơ thể người</a>
            <a href="/" className="list-group-item">Nghiên cứu lâm sàn tác dụng của ĐTHT</a>
            <a href="/" className="list-group-item">Quy trình sản xuất</a>
            <a href="/" className="list-group-item">Quy trình chế biến</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;