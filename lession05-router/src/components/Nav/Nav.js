import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class Nav extends Component {
  render() {
    return (
      <nav className="container">
            <ul className="nav navbar-nav">
              <li><NavLink activeClassName="active" to="/">Trang chính</NavLink></li>
              <li><NavLink activeClassName="active" to="/product">Sản phẩm</NavLink></li>
              <li><NavLink activeClassName="active" to="/contact">Liên hệ</NavLink></li>
            </ul>
      </nav>
    );
  }
}

export default Nav;