import React, {Component} from 'react';
import myData from './../Product/data.json';

class Detail extends Component {
  format_currency = (price) => {
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  }

  render() {
    var pid = parseInt(this.props.match.params.id, 10);
    return (
      <div>
        <div className="col-xs-4 col-md-4 col-lg-4 ta-ct">
          {
            myData.map((val, key) => {
              if ((val.id) === pid) {
                return <div key={key}>
                  <h3> {val.name}</h3>
                  <img className="group list-group-image ojf center" src={val.images} alt=""/>
                  <h3>{this.format_currency(val.price)}</h3>
                  <button className="btn btn-success">Đặt hàng</button>
                </div>;
              }
              return '';
            })
          }
        </div>
        <div className="col-xs-8 col-md-8 col-lg-8">
          Có thể nói <strong> Đông trùng hạ thảo</strong> và <strong>mật ong</strong> là hai dược phẩm mang đến nhiều
          lợi
          ích cho sức khoẻ con người. Nếu kết hợp cả hai thành một thì đây sẽ là món thực phẩm mang giá trị dinh dưỡng
          cao. Hãy xem qua Mật ong Đông trùng hạ thảo sẽ mang đến gì cho sức khoẻ của mình nhé.
          <h3>Lợi ích đến từ Đông trùng hạ thảo và mật ong</h3>
          <p className=" ta-ct"><img className="group list-group-image ojf1"
               src="./../../images/tac-dung-cua-nam-dong-trung-ha-thao-nhan-tao-ngam-mat-ong.jpg" alt=""/></p>
          <ol>
            <li>Theo các nghiên cứu khoa học cho thấy Đông trùng hạ thảo chứa rất nhiều hoạt chất. Trong sinh khối của nó có đến 17 loại axit amin khác nhau. Ngoài ra còn chứa D-mannitol, Lipit và nhiều nguyên tố vi lượng khác nha Al, Si, K, Na,… Hơn nữa, Đông trùng hạ thảo còn có chứa nhiều loại vitamin (vitamin B12, A, C, B2, E, K,…). Đặc biệt, trong sinh khối Đông trùng hạ thảo có nhiều chất hoạt động sinh học mà các nhà khoa học đang phát hiện dần dần ra nhờ các tiến bộ của ngành hoá học các hợp chất tự nhiên.</li>
            <li>Với nhiều hoạt chất có giá trị dược liệu cao, Đông trùng hạ thảo giúp bồi bổ cơ thể, kích thích hệ miễn dịch, cải thiện chức năng sinh lý và hỗ trợ điều trị các bệnh về tim mạch (, gan, thận, phổi và ung thư. Ngoài ra, đối với phụ nữ vị thuốc này giúp chống lão hoá và làm đẹp da.</li>
            <li>Mật ong có khả năng cung cấp năng lượng cần thiết cho cơ thể nhờ vào lượng đường tự nhiên dồi dào của nó. Bao gồm 38,2% fructose, 31,3% glucose, 7,1% maltose, 1,3% sucrose, 17,2% nước và các chất khác. Lượng đường glucose và fructose khiến cho các cơ bắp được phục hồi và dẻo dai hơn. Ngoài ra, mật ong cũng giúp chống nhiễm khuẩn, làm sạch vòm họng và đường ruột. Nếu siêng năng sử dụng mật ong mỗi ngày, da dẻ sẽ trở nên hồng hào hơn, ăn ngon miệng hơn và ngủ ngon giấc hơn.</li>
          </ol>
          <h3>Cách dùng Mật ong Đông trùng hạ thảo</h3>
          <ol>
            <li>Đối với Mật ong Đông trùng hạ thảo, mỗi ngày nên uống làm 03- 04 lần, mỗi lần từ 03- 05 mL. Người dùng có thể pha chung với nước ấm để dễ uống hơn, ngậm tan từ từ trong miệng</li>
            <li>Thời gian tốt nhất nên sử dụng là dùng trước khi buổi ăn 1 giờ, trước khi đi ngủ 1 giờ.</li>
            <li>Để mang lại hiệu quả tốt nhất nên uống Mật ong Đông trùng hạ thảo đều đặn và liên tục mỗi ngày.</li>
          </ol>
          <h3>Cách bảo quản Mật ong Đông trùng hạ thảo</h3>
          <ol>
            <li>Trong lọ thuỷ tinh và có nắp đậy kín</li>
            <li>Hạn chế không khí tràn vào.</li>
            <li>Nhiệt độ bảo quản: bình thường.</li>
            <li>Lưu ý: nhiệt độ quá lạnh sẽ khiến mật ong bị kết tinh.</li>
          </ol>
        </div>
        <br/>
      </div>
    );
  }
}

export default Detail;