import React, {Component} from 'react';

class Body extends Component {
  render() {
    return (
      <div className="container-fluid page-body">
        <div className="container">
          <div className="col-md-12 p0">
            <div className="col-md-6">
              <div className="col-md-12 p0">
                <div className="panel panel-warning">
                  <div className="panel-heading">Thông báo, cảnh báo</div>
                  <ul className="list-group">
                    <li className="list-group-item">Thông báo 1</li>
                    <li className="list-group-item">Thông báo 2</li>
                    <li className="list-group-item">Thông báo 3</li>
                    <li className="list-group-item">Thông báo 4</li>
                    <li className="list-group-item">Thông báo 5</li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="col-md-12 p0">
                <div className="panel panel-info">
                  <div className="panel-heading">Liên kết</div>
                  <div className="panel-body text-center">
                    <div className="col-md-12">
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Lịch làm việc</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Chức năng - nhiệm vụ</button>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Công văn - văn bản</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Tin tức hoạt động</button>
                      </div>
                    </div>
                    <div className="col-md-12">
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Cơ cấu tổ chức</button>
                      </div>
                      <div className="col-md-6">
                        <button className="btn btn-warning btn-links">Các quyết định quan trọng</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-12">
            <div className="panel panel-success">
              <div className="panel-heading text-center">
                <b>MỤC TIÊU CỦA ỦY BAN TƯ PHÁP</b>
              </div>
              <div className="panel-body">
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-12">
            <div className="panel panel-success">
              <div className="panel-heading text-center">
                <b>THỐNG KÊ CÁC CHỈ SỐ</b>
              </div>
              <div className="panel-body">
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án luật, pháp lệnh hình sự
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án luật, pháp lệnh tố tụng hình sự
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
                <div className="col-md-3">
                  <div className="panel panel-success">
                    <div className="panel-heading">
                      Thẩm tra dự án luật, pháp lệnh tố tụng hình sự
                    </div>
                    <div className="panel-body">
                      <h2 style={{display: 'inline'}}>53</h2>
                      <label style={{marginLeft: 10}}>Dự án</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-12">
            <div className="col-md-12 border-gray bg-white p10">
              <button className="btn btn-success" style={{width: '100%'}}>BÁO CÁO</button>
              <button className="btn btn-success mt10" style={{width: '100%'}}>Chọn loại và định dạng báo cáo</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Body;