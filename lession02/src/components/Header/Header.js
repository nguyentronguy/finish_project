import React, {Component} from 'react';

class Header extends Component {
  render() {
    return (
      <div className="page-header text-center">
        <img src="img/header-bg.gif"/>
        <nav className="navbar navbar-default">
          <div className="container">
            {/* Brand and toggle get grouped for better mobile display */}
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                      data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"/>
                <span className="icon-bar"/>
                <span className="icon-bar"/>
              </button>
              <a className="navbar-brand" href="#">
                <span className="fa fa-home"/>
              </a>
            </div>
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav">
                <li className="active">
                  <a href="#">Giới thiệu
                    <span className="sr-only">(current)</span>
                  </a>
                </li>
                <li>
                  <a href="#">Hoạt động</a>
                </li>
              </ul>
              <div className="navbar-form navbar-right">
                <ul className="nav navbar-nav">
                  <li className>
                    <a href="#">Giới thiệu
                      <span className="sr-only">(current)</span>
                    </a>
                  </li>
                  <li className>
                    <a href="#">Đăng nhập
                    </a>
                  </li>
                  <li>
                    <a href="#">Liên hệ</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;