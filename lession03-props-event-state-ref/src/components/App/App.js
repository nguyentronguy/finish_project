import React, {Component} from 'react';
import './App.css';
import Product from "../Product/Product";
import Modal from "../Modal/Modal";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [
        {
          name: "Laptop Asus",
          price: "10000000",
          images: "/images/p1.JPG",
          hot: true
        },
        {
          name: "Cellphone Asus",
          price: "5000000",
          images: "/images/p2.JPG",
          hot: false
        },
        {
          name: "iPhone X",
          price: "15000000",
          images: "/images/p3.JPG",
          hot: true
        },
        {
          name: "Macbook",
          price: "35000000",
          images: "images/p3.JPG",
          hot: false
        },
        {
          name: "Laptop HP",
          price: "15000000",
          images: "/images/p4.JPG",
          hot: true
        }
      ]
    };
  }

  deleteProduct = (id) => {
    var arrayProduct = this.state.products;
    arrayProduct.splice(id, 1);
    this.setState({products: arrayProduct});
  }

  editNameProduct = (id, name) => {
    var arrayProduct = this.state.products;
    arrayProduct[id].name = name;
    this.setState({products: arrayProduct});
  }

  show_product = () => {
    const listProduct = this.state.products.map((item, index) =>
      <Product key={index} index={index} price={item.price} edit={(id, name) => {
        this.editNameProduct(id, name)
      }} delete={(id) => {
        this.deleteProduct(id)
      }} image={item.images} hot={item.hot}>{item.name}</Product>
    );
    return listProduct;
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          {this.show_product()}
          <Modal/>
        </div>
      </div>
    );
  }
}

export default App;
