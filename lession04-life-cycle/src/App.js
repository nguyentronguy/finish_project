import React, {Component} from 'react';
import './App.css';
import Content from "./content";

class App extends Component {
  constructor(props) {
    super(props);
    console.log("init");
    this.state = {
      component: "Component Init",
      fullname: "Tuan"
    };
  }

  componentWillMount() {
    console.log("componentWillMount");
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  updateState = () => {
    this.setState({
      component: "New State",
      fullname: "Teo"
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate: " + nextState.component);
    return true;
  }

  componentWillUpdate(nextProps, nextState) {
    console.log("componentWillUpdate: " + nextState.component);
  }

  componentDidUpdate(prevProps, prevState) {
    console.log("componentDidUpdate: " + prevState.component);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount:")
  }

  render() {
    console.log("render");
    console.log(this.state.component);
    return (
      <div className="App">
        <Content name={this.state.fullname}/>
        <button onClick={() => this.updateState()}> Click me</button>
      </div>
    );
  }
}

export default App;
