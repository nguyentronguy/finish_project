import React, {Component} from 'react';

class Content extends Component {
  componentWillReceiveProps(nextProps) {
console.log("componentWillReceiveProps: "+nextProps.name)
  }

  shouldComponentUpdate(nextProps, nextState) {
console.log("shouldComponentUpdate: "+nextProps.name);
    return true;
  }


  componentWillUpdate(nextProps, nextState) {
console.log("componentWillUpdate: "+nextProps.name)
  }


  render() {
    return (
      <div>
        <div>{this.props.name}</div>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
console.log("componentDidUpdate: "+prevProps.name)
  }
}


export default Content;