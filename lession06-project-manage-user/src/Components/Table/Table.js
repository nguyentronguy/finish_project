import React, {Component} from 'react';
import TableRow from "./TableRow";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

class Table extends Component {

  showButton = () => {
    if (!this.props.statusForm) {
      return <a className="btn btn-success btn-sm" href="them" role="button"
                onClick={(e) => this.props.formToggle(e)}><FontAwesomeIcon icon="plus"/>Them
      </a>

    } else {
      return <a className="btn btn-danger btn-sm" href="dong" role="button"
                onClick={(e) => this.props.formToggle(e)}><FontAwesomeIcon icon="times"/> Dong
      </a>
    }
  }

  classTable = () => {
    if (!this.props.statusForm) {
      return "col-xs-12 col-sm-12 col-md-12 col-lg-12"

    } else {
      return "col-xs-8 col-sm-8 col-md-8 col-lg-8"
    }
  }

  mappingData = () => {
    const tableRow = this.props.userData.map((value, key) => {
      return <TableRow key={key} level={value.level} index={key + 1}>{value.user} </TableRow>
    })
    return tableRow;
  }

  render() {
    console.log(this.props.userData);
    return (
      <div className={this.classTable()}>
        <table className="table table-bordered table-hover">
          <thead>
          <tr>
            <th className="col-ms-1">STT</th>
            <th className="col-ms-5">Thanh vien</th>
            <th className="col-ms-2">Quyen</th>
            <th className="col-ms-4 text-center" colSpan={2}>
              {this.showButton()}
            </th>
          </tr>
          </thead>
          <tbody>
          {this.mappingData()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;