import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class TableRow extends Component {
  render() {
    return (
      <tr>
        <td>{this.props.index}</td>
        <td>{this.props.children}</td>
        <td>{(this.props.level===1)?"Quan ly":"Thanh vien"}</td>
        <td className="text-center">
          <a className="btn btn-warning btn-sm" href="sua" role="button">
              <FontAwesomeIcon icon="edit" aria-hidden="true"/> Sua
          </a>
        </td>
        <td className="text-center">
          <a className="btn btn-danger btn-sm" href="xoa" role="button">
              <FontAwesomeIcon icon="trash" aria-hidden="true"/> Xoa
          </a>
        </td>
      </tr>
    );
  }
}

export default TableRow;