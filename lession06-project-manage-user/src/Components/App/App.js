import React, {Component} from 'react';
import Nav from "../Nav/Nav";
import Table from "../Table/Table";
import Form from "../Form/Form";
import {library} from '@fortawesome/fontawesome-svg-core';
import {faEdit, faPlus, faTimes, faTrash} from '@fortawesome/free-solid-svg-icons';
import data from '../../data.json';

library.add(faEdit, faPlus, faTimes, faTrash)

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      statusForm: false,
      userData: data
    };
  }

  showForm = () => {
    if (this.state.statusForm) {
      return <Form formToggle={(e) => this.changeStatusForm(e)} add={(item)=>this.addAction(item)}/>
    }
  }

  changeStatusForm = (event) => {
    event.preventDefault();
    this.setState({
      statusForm: !this.state.statusForm
    });
  }

  addAction =(item)=>{
      this.state.userData.push(item);

    this.setState({
      userData:this.state.userData
    });
  }

  render() {
    console.log(this.state.userData);
    return (
      <div className="App">
        <div className="container" id="main-content">
          <Nav/>
          <hr/>
          <div className="row">
            <Table userData={this.state.userData} statusForm={this.state.statusForm}
                   formToggle={(e) => this.changeStatusForm(e)}></Table>
            {this.showForm()}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
