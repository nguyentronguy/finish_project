import React, {Component} from 'react';

class Nav extends Component {
    render() {
        return (
            <div>
                <div id="frm-top" className="btn-group btn-group-toggle"  data-toggle="buttons">
                  <div>
                    <label className="btn btn-secondary">
                        <input type="radio" name="options" id="option2" autoComplete="off"/> Trang chu
                    </label>
                    <label className="btn btn-secondary">
                        <input type="radio" name="options" id="option3" autoComplete="off"/> The loai
                    </label>
                    <label className="btn btn-secondary">
                        <input type="radio" name="options" id="option3" autoComplete="off"/> San Pham
                    </label>
                  </div>
                    <form className="form-inline pull-right">
                        <input className="form-control" type="text" placeholder="Nhap tu khoa"/>
                        <button className="btn btn-outline-success " type="submit">Tim kiem
                        </button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Nav;