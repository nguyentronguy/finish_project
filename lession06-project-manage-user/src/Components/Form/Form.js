import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
const uuidv4 = require('uuid/v4');

class Form extends Component {

  constructor(props) {
    super(props);
    this.state = {
      txtUser: '',
      txtPass: '',
      sltLevel: 1
    };
  }


  changeInput = (event) => {
    const target = event.target;
    const name = target.name;
    const value = target.value;

    this.setState({
      [name]: value
    });
  }
  submitForm = (event) => {
    event.preventDefault();
    event.target.reset();

    const {txtUser, txtPass, sltLevel} = this.state;

    const item = {};
    item.id = uuidv4();;
    item.user = txtUser;
    item.password = txtPass;
    item.level = parseInt(sltLevel, 10);
    this.props.add(item);
  };


render()
{
  return (
    <div className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div className="alert alert-danger" role="alert"><strong>Loi:</strong> Vui long nhap thong tin
      </div>
      <div className="alert alert-success" role="alert"><strong>Thong bao:</strong> Thanh cong
      </div>
      <div className="card-header">
        Them
        <button type="button" className="close" aria-label="Close" onClick={(e) => this.props.formToggle(e)}>
          <span aria-hidden="true"><FontAwesomeIcon icon="times"/></span>
        </button>
      </div>
      <div className="card[-block">
        <form method="POST" onSubmit={(e) => this.submitForm(e)}>
          <div className="form-group">
            <label htmlFor="txtUser">Thanh vien</label>
            <input type="text" name="txtUser" className="form-control" placeholder="Nhap thanh vien"
                   onChange={(e) => this.changeInput(e)}/>
          </div>
          <div className="form-group">
            <label htmlFor="txtPass">Mat khau</label>
            <input type="password" name="txtPass" className="form-control" placeholder="Nhap mat khau"
                   onChange={(e) => this.changeInput(e)}/>
          </div>
          <div className="form-group">
            <label htmlFor="sltLevel">Quyen</label>
            <select className="form-control" name="sltLevel" value={this.state.sltLevel}
                    onChange={(e) => this.changeInput(e)}>
              <option value={1}>Quan tri vien</option>
              <option value={2}>Thanh vien</option>
            </select>
          </div>
          <button type="submit" className="btn btn-success btn-sm"><FontAwesomeIcon icon="plus"/>Them
          </button>
        </form>
      </div>
    </div>
  );
}
}

export default Form;